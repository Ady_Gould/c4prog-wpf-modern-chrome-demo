ModernChrome-Demo
=====

IMPORTANT
----

You must install the ModernChrome NuGet package for this application to work.

https://github.com/ChristianIvicevic/ModernChrome

Easiest way is to open the NuGet Console (Tools -> Nuget Package Manager -> NuGet Package Console) and paste in:

Install-Package ModernChrome -Version 1.0.0-alpha001

